Weblock with softshackle for 25mm rodeolines with a webbing thickness of ca. 3mm (The model needs to be adjusted for other webbing thicknesses). With 44g it's probably the lightest weblock with connector where you can still adjust the length of your webbing. Don't use it in tensioned lines.

Parts:

    Printed part
    Aluminium tube 19,5x1,5x500 mm (for 7mm 19,5x1,5x540mm)
    3mm soft shackle

Assembly:

    Sand your weblock body nice and smooth, otherwise your soft shackle will get caught
    Heat the printed body in a oven (depending on the material, 75°C works for me with PET) and press the aluminium tube inside. I broke multiple bodies without heating.
    Soft shackle: I use 800mm of 3mm dyneema with a button knot. Tail buries are 60mm. Instructions are here https://www.youtube.com/watch?v=91_jEjQdlBU
    Put the softshackle through one of the holders on the side. The noose should point to the back.

Use:

    Put the noose of the soft shackle through the sling and then through the holder on the other side.
    Insert the webbing like in a normal weblock
    The soft shackle is also the pin of the weblock. Feed it through the webbing
    Close the soft shackle

Breaktests

    The weblock broke at 2,8kN (4,4kN with 7mm walls) printed with PET. Only the side plates break, the weblock still holds the webbing
    The soft shackle broke at 17kN
    Thanks for the breaktest slacktivity

